@extends('layouts.app')

@section('title', 'Create details')

@section('content')
        <h1>Candidate details</h1>
        <form method = "post" action = "{{action('CandidatesController@details',$candidate->id)}}">
        @csrf
        @METHOD('PATCH')  
        <div class="form-group">
        <label>Candiadte name: {{$candidate->name}}</label>
        </div>     
        <div class="form-group">
        <label>Candiadte email: {{$candidate->email}}</label>
        </div>
        <div class="form-group">
            <label>Candiadte owner:</label>
            @if(isset($candidate->user_id))
                {{$candidate->owner->name}}  
            @else
                don't have 
            @endif
        </div> 
        <div class="form-group">
            <label>Candiadte status:</label>
            <div class="dropdown">
                    @if (null != App\Status::next($candidate->status_id))    
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        @if (isset($candidate->status_id))
                           {{$candidate->status->name}}
                        @else
                            Define status
                        @endif
                    </button>
                    @else 
                    {{$candidate->status->name}}
                    @endif
                                                   
                    @if (App\Status::next($candidate->status_id) != null )
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        @foreach(App\Status::next($candidate->status_id) as $status)
                         <a class="dropdown-item" href="{{route('candidates.changestatus', [$candidate->id,$status->id])}}">{{$status->name}}</a>
                        @endforeach                               
                    </div>
                    @endif
                      
        </form>    
    </body>
</html>
@endsection

